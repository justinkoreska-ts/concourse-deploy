#!/bin/sh -e

CONCOURSE_VERSION=6.5.1
CONCOURSE_PLATFORM=linux-amd64

cd /opt

echo downloading Concourse/$CONCOURSE_VERSION ...
wget -q https://github.com/concourse/concourse/releases/download/v$CONCOURSE_VERSION/concourse-$CONCOURSE_VERSION-$CONCOURSE_PLATFORM.tgz
echo unpacking ...
tar zxf concourse-$CONCOURSE_VERSION-$CONCOURSE_PLATFORM.tgz

cd concourse

bin/concourse generate-key -t rsa -f session_signing_key
bin/concourse generate-key -t ssh -f tsa_host_key
bin/concourse generate-key -t ssh -f worker_key
cp worker_key.pub authorized_worker_keys

echo creating and starting services ...
cat <<EOF > /etc/systemd/system/concourse-web.service
[Unit]
Description=Concourse CI Web $CONCOURSE_VERSION
After=network.target network-online.target 
[Service]
Restart=on-failure
StartLimitBurst=5
WorkingDirectory=/opt/concourse
ExecStart=/opt/concourse/bin/concourse web
Environment=CONCOURSE_ADD_LOCAL_USER=admin:Trip2020!
Environment=CONCOURSE_MAIN_TEAM_LOCAL_USER=admin
Environment=CONCOURSE_SESSION_SIGNING_KEY=session_signing_key
Environment=CONCOURSE_TSA_HOST_KEY=tsa_host_key
Environment=CONCOURSE_TSA_AUTHORIZED_KEYS=authorized_worker_keys
Environment=CONCOURSE_POSTGRES_HOST=127.0.0.1
Environment=CONCOURSE_POSTGRES_PORT=5432
Environment=CONCOURSE_POSTGRES_DATABASE=concourse
Environment=CONCOURSE_POSTGRES_USER=concourse
Environment=CONCOURSE_POSTGRES_PASSWORD=Trip2020!
Environment=CONCOURSE_EXTERNAL_URL=http://concourse.local:18080
EOF

cat <<EOF > /etc/systemd/system/concourse-worker.service
[Unit]
Description=Concourse CI Worker $CONCOURSE_VERSION
After=network.target network-online.target 
[Service]
Restart=on-failure
StartLimitBurst=5
WorkingDirectory=/opt/concourse
ExecStart=/opt/concourse/bin/concourse worker
ExecStop=/opt/concourse/bin/concourse land-worker
ExecStop=/opt/concourse/bin/concourse retire-worker
Environment=CONCOURSE_WORK_DIR=work
Environment=CONCOURSE_TSA_HOST=127.0.0.1:2222
Environment=CONCOURSE_TSA_PUBLIC_KEY=tsa_host_key.pub
Environment=CONCOURSE_TSA_WORKER_PRIVATE_KEY=worker_key
Environment=CONCOURSE_GARDEN_DNS_SERVER=8.8.8.8
EOF

systemctl daemon-reload
systemctl start concourse-web
systemctl start concourse-worker

echo done!
